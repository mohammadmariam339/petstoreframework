Feature:As a QA, I validate the Swagger API CRUD operation

  @api
  Scenario Outline: validate pet and user
    Given Create a pet with the following data and send a POST request
      | categoryID   | <categoryID> |
      | categoryName | Wolf         |
      | petName      | Wolf         |
      | photoURL     | my pet URL   |
      | tagID        | 85           |
      | tagName      | Wolf         |
      | status       | available    |
    And Validate that status code is 200

    And I send a PUT request to with the following data:
      | categoryID   | <categoryID> |
      | categoryName | Wolf         |
      | petName      | <petName>    |
      | photoURL     | my pet URL   |
      | tagID        | 85           |
      | tagName      | <tagName>    |
      | status       | <status>     |

    And Validate that status code is 200
    And I send a GET request to "/v2/pet/"
    And I send a GET request to "/v2/pet/"
    When I send a DELETE request to "/v2/pet/"

    Examples:
      | categoryID | petName | tagName | status    |
      | 85         | Wolfy   | Wolfy   | available |
