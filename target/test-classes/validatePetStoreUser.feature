Feature: As a QE, I should be able to test Pet Store User Information

  @api
  Scenario: Validate Pet Store User
    Given Create data request to for the user
      | id         | 85              |
      | username   | Mariam          |
      | firstName  | Mariam          |
      | lastName   | Mohammad        |
      | email      | mm@gmail.com    |
      | password   | MMMMMM85858585  |
      | phone      | (000) 000 - 000 |
      | userStatus | 58              |
    And Validate the status code to be 200 for the user