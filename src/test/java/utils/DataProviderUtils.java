package utils;

import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

public class DataProviderUtils {

        @DataProvider(name = "DataFromExcel")
        public static Object[][] getDataFromExcelWithDataProvider(Method method){
            //Open file
            ExcelUtils.openExcelFile("PetsData", "Sheet1");
            //Get the values by converting the list of list into multidimensional Object array
//            Object[][] arrayObject = ExcelUtils.getExcelData(ExcelUtils.getValues());
            Object[][] arrayObject = ExcelUtils.getExcelData(ExcelUtils.getValues(method.getName()));
            //Close the file
            ExcelUtils.closeExcelFile();

            return arrayObject;
        }
}
