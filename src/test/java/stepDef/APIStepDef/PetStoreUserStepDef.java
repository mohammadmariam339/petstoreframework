package stepDef.APIStepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pojoClasses.users.PetStoreUser;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.ConfigReader.getProperty;

public class PetStoreUserStepDef {

    Response response;

    @Given("Create data request to for the user")
    public void createDataRequestToForTheUser(Map<String, String> petUser) {
        PetStoreUser petStoreUser = PetStoreUser.builder()
                .id(Integer.parseInt(petUser.get("id")))
                .username(petUser.get("username"))
                .firstName(petUser.get("firstName"))
                .lastName(petUser.get("lastName"))
                .email(petUser.get("email"))
                .password(petUser.get("password"))
                .phone(petUser.get("phone"))
                .userStatus(Integer.parseInt(petUser.get("userStatus")))
                .build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(petStoreUser)
                .post(getProperty("petStoreURL") + "/v2/pet")
                .then().extract().response();
    }


    @And("Validate the status code to be {int} for the user")
    public void validateTheStatusCodeToBeForTheUser(int statusCode) {

        int actualStatusCode = response.getStatusCode();

        assertThat(
                "I am expecting status code: " + statusCode,
                actualStatusCode,
                is(statusCode)
        );
    }

}