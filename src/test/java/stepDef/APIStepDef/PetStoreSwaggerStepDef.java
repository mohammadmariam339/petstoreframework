package stepDef.APIStepDef;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pojoClasses.PetStoreHomework.Category;
import pojoClasses.PetStoreHomework.CreatePet;
import pojoClasses.PetStoreHomework.Tags;
import utils.CommonUtils;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.ConfigReader.getProperty;

public class PetStoreSwaggerStepDef {
    Response response;
    int actualPetId;

    @Given("Create a pet with the following data and send a POST request")
    public void createAPetWithTheFollowingDataAndSendAPOSTRequestTo(Map<String, String> petData) {
        Category category = Category.builder()
                .id(Integer.parseInt(petData.get("categoryID")))
                .name(petData.get("categoryName")).build();

        Tags tags = Tags.builder()
                .id(Integer.parseInt(petData.get("tagID")))
                .name(petData.get("tagName")).build();

        CreatePet createPet = CreatePet.builder()
                .id(2)
                .name(petData.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petData.get("photoURL")))
                .tags(Collections.singletonList(tags))
                .status(petData.get("status")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(createPet)
                .post(getProperty("petStoreURL") + "/v2/pet")
                .then().log().all().assertThat().statusCode(200)
                .extract().response();

        actualPetId = JsonPath.read(response.asString(), "id");
    }

    @And("Validate that status code is {int}")
    public void validateThatStatusCodeIs(int statusCode) {

        int actualStatusCode = response.getStatusCode();

        assertThat(
                "I am expecting status code: " + statusCode,
                actualStatusCode,
                is(statusCode)
        );
    }

    @And("I send a PUT request to with the following data:")
    public void iSendAPUTRequestToWithTheFollowingData(Map<String, String> petData) {

        Category category = Category.builder()
                .id(Integer.parseInt(petData.get("categoryID")))
                .name(petData.get("categoryName")).build();

        Tags tags = Tags.builder()
                .id(Integer.parseInt(petData.get("tagID")))
                .name(petData.get("tagName")).build();

        CreatePet createPet = CreatePet.builder()
                .id(CommonUtils.genRandomNumber(2))
                .name(petData.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petData.get("photoURL")))
                .tags(Collections.singletonList(tags))
                .status(petData.get("status")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(createPet)
                .post(getProperty("petStoreURL") + "/v2/pet")
                .then().extract().response();

        actualPetId = JsonPath.read(response.asString(), "id");
    }


    @And("I send a GET request to {string}")
    public void iSendAGETRequestTo(String getUrl) {

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(getProperty("petStoreURL") + getUrl + actualPetId)
                .then().extract().response();
    }

    @When("I send a DELETE request to {string}")
    public void iSendADELETERequestTo(String deleteUrl) {
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(getProperty("petStoreURL") + deleteUrl)
                .then().extract().response();
    }

}